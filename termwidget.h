#ifndef TERMWIDGET_H
#define TERMWIDGET_H

#include "/usr/include/qtermwidget5/qtermwidget.h"


class TermWidget : public QTermWidget
{
public:
  TermWidget(int argc, char *argv[]);

  static void activateUrl(const QUrl &url, bool fromContextMenu);
};

#endif // TERMWIDGET_H
