#include "termwidget.h"

#include <QApplication>
#include <QDesktopServices>

TermWidget::TermWidget(int argc, char *argv[])
{
  /*
  for (int i = 0; i < argc; i++) {
    std::cout << "for loop\n";
    if (argv[i] == "-d") {
      std::cout << "-d parsed\n" << argv[i+1];
      this->setWorkingDirectory(argv[i+1]);
    }
  }
  */
  QCursor cursor;
  cursor.setShape(Qt::CursorShape::IBeamCursor);
  this->setCursor(cursor);

  this->setTerminalOpacity(0.5);
  //this->setScrollBarPosition(QTermWidget::ScrollBarRight);
  /*
   * Possible color schemes:
   * ("BlackOnRandomLight", "Tango", "Ubuntu",
   *  "BlackOnWhite", "Linux", "DarkPastels",
   *  "BlackOnLightYellow", "SolarizedLight",
   *  "GreenOnBlack", "Solarized", "WhiteOnBlack",
   *  "BreezeModified")
   */
  this->setColorScheme("Linux");
  this->setShellProgram(getenv("SHELL"));
  this->setEnvironment({"TERM","xterm-256color"});
  this->setAttribute(Qt::WA_NoSystemBackground, true);
  this->setAttribute(Qt::WA_TranslucentBackground, true);
  this->setStyleSheet("background:transparent;");
  this->setWindowFlags(Qt::FramelessWindowHint);
}

void TermWidget::activateUrl(const QUrl &url, bool fromContextMenu) {
  if (QApplication::keyboardModifiers() & Qt::ControlModifier || fromContextMenu) {
    QDesktopServices::openUrl(url);  // std::system("firefox url &");
  }
}
