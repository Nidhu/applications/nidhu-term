#include <QApplication>
#include <QKeySequence>
#include <QMainWindow>
#include <QGraphicsBlurEffect>

#include <iostream>
#include <stdio.h>
#include <unistd.h>

#include "termwidget.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  QMainWindow *mainWindow = new QMainWindow();
  TermWidget *console = new TermWidget(argc, argv);

  mainWindow->resize(600, 400);

  if (setenv("TERM", "xterm-256color", true) < 0) {
    std::cout << "Couldn't set $TERM variable \n";
  }

	/*
  if (std::system("export TERM=\"xterm-256color\"") != 0) {
    std::cout << "Couldn't set $TERM variable \n";
  }
  */

  QFont font = QApplication::font();
  font.setFamily("Hack");
  font.setPointSize(10);
  console->setTerminalFont(font);

  QPalette pal;
  pal.setColor(QPalette::Foreground, Qt::black);
  mainWindow->setPalette(pal);

	// FIXME
  QObject::connect(console, &QTermWidget::termKeyPressed, mainWindow, [=](const QKeyEvent *key) -> void {
      if (key->matches(QKeySequence::Copy)) {
        console->copyClipboard();
      } else if (key->matches(QKeySequence::Paste)) {
        console->pasteClipboard();
      }
    });
  QObject::connect(console, &QTermWidget::urlActivated, mainWindow, TermWidget::activateUrl);
  QObject::connect(console, SIGNAL(finished()), mainWindow, SLOT(close()));

  mainWindow->setCentralWidget(console);
  mainWindow->show();

  return a.exec();
}
